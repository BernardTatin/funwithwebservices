#!/usr/bin/env bash

script=$(basename $0)
here=$(cd $(dirname $0) ; pwd)

source ${here}/common.sh
source ${here}/jdk-list.sh

function dohelp() {
    local ecode=0
    [[ $# -ne 0 ]] && ecode=$1 && shift

    cat <<DOHELP
${script} -h|--help: this text
${script} [OPTIONS ...]

OPTIONS:
    --jdk8|jdk9|jdk10: jdk to compile and run the services, default jdk8
DOHELP
    exit ${ecode}
}

jdk=${jdk8}
opt="${opt8}"

while [[ $# -ne 0 ]]
do
    case $1 in
        -h|--help)
            dohelp
            ;;
        --jdk8)
            jdk=$jdk8
            opt="${opt8}"
            ;;
        --jdk9)
            jdk=$jdk9
            opt="${opt9}"
            ;;
        --jdk10)
            jdk=$jdk10
            opt="${opt10}"
            ;;
        --jdk11)
            ## onError 3 "Cannot compile with jdk 11"
            jdk=$jdk11
            opt="${opt11}"
            ;;
        *)
            onError 1 "Unknown command"
            ;;
    esac
    shift
done

setJDK $jdk

echo "JAVA_HOME: $JAVA_HOME"
echo "PATH     : $PATH"
javac -version
java -version

mkdir -p ${base}/out/production/FunWithWebServices
cd ${base}/src/ || onError 1 "Can't cd"
javac -d ${base}/out/production/FunWithWebServices ${opt} $(find btat1 -name '*.java') || onError 1 "Can't compile"

cd ${base}/out/production/FunWithWebServices || onError 1 "Can't cd"
java ${opt} btat1.webservices.fun.UCPublisher
