#!/usr/bin/env bash

jdk8=/usr/lib/jvm/java-8-openjdk-amd64
opt8=
jdk9=/usr/local/java/jdk-9.0.4
opt9='--add-modules java.xml.ws'
jdk10=/usr/java/jdk-10.0.2
opt10='--add-modules java.xml.ws'
jdk11=/usr/lib/jvm/java-11-openjdk-amd64
opt11='--add-modules java.xml.ws'

function setJDK() {
    local jdk=$1

    export JAVA_HOME=${jdk}
    export PATH=${JAVA_HOME}/bin:${PATH}
}
