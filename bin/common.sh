#!/usr/bin/env bash

base=$(dirname ${here})

function onError() {
    local ecode=1
    [[ $# -ne 0 ]] && ecode=$1 && shift
    echo "${script} ERROR: $@" 1>&2
    exit ${ecode}
}
