#!/usr/bin/env bash

## cf: https://www.quennec.fr/trucs-astuces/systèmes/gnulinux/commandes/internet/curl-appeler-un-webservice-soap
script=$(basename $0)
here=$(cd $(dirname $0) ; pwd)

source ${here}/common.sh

[[ $# -ne 2 ]] && echo "$(basename $0) URL reqest.xml" && exit 0

url="$1"
request="$2"

! [[ -f ${request} ]] && onError "cannot find ${request} file"

curl --silent \
    --header "content-type: text/xml; charset=utf-8" \
    --data @"${request}" \
    "${url}" \
    | xmlindent | grep -w return
