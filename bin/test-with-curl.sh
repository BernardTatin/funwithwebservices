#!/usr/bin/env bash

script=$(basename $0)
here=$(cd $(dirname $0) ; pwd)

source ${here}/common.sh

function dohelp() {
    local ecode=0
    [[ $# -ne 0 ]] && ecode=$1 && shift

    cat <<DOHELP
${script} -h|--help: this text
${script} [OPTIONS ...]

OPTIONS:
    --sleep secondes: time interval between groups of test
    --threads number: number of threads
    --url           : URL of webservices
DOHELP
    exit ${ecode}
}

function send_request() {
    local request=$1

   curl --silent \
    --header "content-type: text/xml; charset=utf-8" \
    --data @"${request}" \
    "${url}" \
    | xmlindent | grep -w return &
}

url=http://localhost:9901/UC
sleep=2
threads=5

while [[ $# -ne 0 ]]
do
    case $1 in
        -h|--help)
            dohelp 0
            ;;
        --sleep)
            shift
            [[ $# -eq 0 ]] && onError 1 "--sleep needs an argument, try ${script} --help"
            sleep=$1
            ;;
        --threads)
            shift
            [[ $# -eq 0 ]] && onError 1 "--threads needs an argument, try ${script} --help"
            threads=$1
            ;;
        --url)
            shift
            [[ $# -eq 0 ]] && onError 1 "--url needs an argument, try ${script} --help"
            url=$1
            ;;
        *)
            onError 2 "Unknown argument, try ${script} --help"
            ;;
    esac
    shift
done

while true
do
    ithreads=0
    while [[ $ithreads -lt $threads ]]
    do
        for f in ${base}/data/*.xml
        do
            send_request $f
            ithreads=$(( ithreads + 1 ))
            [[ $ithreads -ge $threads ]] && break
        done
    done
    sleep ${sleep}
done
