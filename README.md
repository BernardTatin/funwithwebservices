# FunWithWebServices

I need to learn more about Web Services written in Java. So I ask
to _DuckDuckGo_ how to do it and he gave me that nice link
[Web services in Java SE, Part 2: Creating SOAP web services](https://www.javaworld.com/article/3215966/java-language/web-services-in-java-se-part-2-creating-soap-web-services.html).
In a few minutes, I get an _IntelliJ IDEA_ project which works.

## testing

I suppose your fluent enough  with programming, to get this project running with _IntelliJ IDEA_, _Eclipse_, _Netbeans_... Without any modifications, once running, you can get the _wsdl_ definition file at this URL: [http://localhost:9901/UC?wsdl](http://localhost:9901/UC?wsdl). 
