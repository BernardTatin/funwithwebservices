package btat1.webservices.fun;

import btat1.webservices.fun.configuration.Defaultalues;

import javax.xml.ws.Endpoint;

public class UCPublisher
{
    static private void doHelp(int exit_code) {
        System.out.println("java -jar funwithwebservices.jar -h|--help: this text");
        System.out.println("java -jar funwithwebservices.jar: run with default url (http://"
                                   + Defaultalues.hostName
                                   + ":" + Defaultalues.port
                                   + "/" + Defaultalues.serviceName);
        System.out.println("java -jar funwithwebservices.jar: OPTIONS");
        System.out.println("    where OPTIONS is at least one of these:");
        System.out.println("    --host hostname, default " + Defaultalues.hostName);
        System.out.println("    --port port, default " + Defaultalues.port);
        System.out.println("    --servicename service, default " + Defaultalues.serviceName);
        java.lang.System.exit(exit_code);
    }

    static private String getURLfromArgs(String[] args) {
        String currentHost = Defaultalues.hostName;
        String currentPort = Defaultalues.port;
        String currentService = Defaultalues.serviceName;
        int argsLen = args.length;

        for (int i = 0; i < argsLen; i++) {
            if ((args[i].compareTo("--help") == 0)
                    || (args[i].compareTo("-h") == 0)) {
                doHelp(0);
            } else if (args[i].compareTo("--host") == 0) {
                i++;
                if (i < argsLen) {
                    currentHost = args[i];
                } else {
                    doHelp(1);
                }
            } else if (args[i].compareTo("--port") == 0) {
                i++;
                if (i < argsLen) {
                    currentPort = args[i];
                } else {
                    doHelp(1);
                }

            } else if (args[i].compareTo("--servicename") == 0) {
                i++;
                if (i < argsLen) {
                    currentService = args[i];
                } else {
                    doHelp(1);
                }
            } else {
                doHelp(1);
            }
        }
        return "http://" + currentHost + ":" + currentPort + "/" + currentService;
    }


    public static void main(String[] args)
    {
        String url = getURLfromArgs(args);

        try {
            Endpoint.publish(url, new UCImpl());
            System.out.println("You can get the WSDL definition at : " + url + "?wsl");
        }
        catch (Exception e) {
            System.err.println("ERROR (" + url + "): " + e.getMessage());
            java.lang.System.exit(1);
        }
    }
}

