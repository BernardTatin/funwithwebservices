package btat1.webservices.fun;

import javax.jws.WebService;

@WebService(endpointInterface = "btat1.webservices.fun.UC")
public class UCImpl implements UC {
    private double mega = 1024 * 1024;

    @Override
    public double c2f(double degrees) {
        // return 1.0 * Runtime.getRuntime().freeMemory() / 1024;
        return degrees * 9.0 / 5.0 + 32;
    }

    @Override
    public double cm2in(double cm) {
        // return 1.0 * Runtime.getRuntime().totalMemory() / 1024;
        return cm / 2.54;
    }

    @Override
    public double f2c(double degrees) {
        // return 1.0 * Runtime.getRuntime().maxMemory() / 1024;
        return (degrees - 32) * 5.0 / 9.0;
    }

    @Override
    public double in2cm(double in) {
        return in * 2.54;
    }
}

